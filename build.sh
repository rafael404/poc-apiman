#!/bin/bash

##start docker
echo "                   -------->Start Build<--------  \n "
docker-compose up -d
echo "Wait... \n"
sleep 30

##create a new organizarions
echo "                   -------->Create a new organizarion<--------   \n"
curl http://172.17.0.2:8080/apiman/organizations/ -d '{"name":"New York Times","description":"A modern American Jornal"}'  -H "Content-Type: application/json" -H "Authorization: Basic YWRtaW46YWRtaW4xMjMh" -v

echo "\n"

##create new books Api 
echo "                   -------->Create new books Api<--------   \n"
curl http://172.17.0.2:8080/apiman/organizations/NewYorkTimes/apis/ -d '{"name":"books-api","description":"Service New York times book list","endpoint":"https://api.nytimes.com/svc/books/v3/","publicAPI":"true","endpointType":"rest","parsePayload":"true","initialVersion":"1","endpointContentType":"json","definitionType":"None"}' -H "Content-Type: application/json" -H "Authorization: Basic YWRtaW46YWRtaW4xMjMh"  -v

echo "\n"

##publish Api 
echo "                   -------->Publish Api<--------   \n"
curl http://172.17.0.2:8080/apiman/actions/ -d '{"type": "publishAPI", "entityId":"books-api","entityVersion":"1","organizationId":"NewYorkTimes"}' -H "Content-Type: application/json" -H "Authorization: Basic YWRtaW46YWRtaW4xMjMh"  -v

echo "\n"

##Request geteway Book-api

echo "                   -------->Request geteway Book-api<--------   \n"

curl 'http://172.17.0.2:8080/apiman-gateway/NewYorkTimes/books-api/1/lists/overview.json?published_date=2011-04-23&api-key=UnRHn0PKG98GeEua3ADEt68y3LqCBtvV'  -H "Content-Type: application/json" 
echo "\n"


##Close Docker 
echo "                   -------->Stopping and removing docker<--------   \n"
docker-compose down 
echo "                   -------->Finish build<--------   "
