# Apiman Poc

Automatização da criação de cliente e endpoint do api manager Apiman.
Criando o cliente endpoint com acesso ao Api do New york times e a publicando deixando o acesso ao usuário pelo Apiman.


# Iniciando

Para executar deve executar o arquivo build.sh.
```bash
sudo sh build.sh
```

## Visualizando UI

Para visualizar a Ui do Apiman deve remover a linha **docker-compose down** do arquivo *build.sh*
e acessar o link **http://172.17.0.2:8080/apimanui/** com credencias de admim padrão. 

usuario: admin
senha: admin123!




## Observações

- O contêiner do Apiman esta sendo executado no ip 172.17.0.2 na  porta 8080.
- apos executar build.sh e querer visualizar os novos dados na UI da Apiman, fazer login acessar nome do usuário (admin) e acessar My Stuff para visualizar client e endpoint.
